package com.example.ahsanislam.androidfirestore;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FireStoreManager {


    private static final String TAG = "FireStoreManagerClass";
    private FirebaseFirestore db;   // Access a Cloud Firestore instance from your Activity
    private List<Map<String, Object>> listOfData;
    private boolean loggedIn;

    /*

     */
    FireStoreManager() {
        db = FirebaseFirestore.getInstance();
        listOfData = new ArrayList<>();
        loggedIn=false;
        loginToFirebase();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);
    }

    private void loginToFirebase() {
        String email = "ahsanislam04@gmail.com";
        String password = "123456";
        // Authenticate with Firebase and subscribe to updates
        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    loggedIn=true;
                    Log.d(TAG, "firebase auth success");
                } else {
                    Log.d(TAG, "firebase auth failed");
                }
            }
        });
    }
    /*
     add data to child of a table
     param1: data to be store in table(Map<String, Object>).
     param2: collectionPath the collection name whome we want to add data(String)
     param3: Collection's childName the child name whome we want to add data (String)
     */
    public void addNestedDataToFirestore(Map<String, Object> data, String collectionPath, String childName) {
        // Add a new document with a generated ID
        db.collection(collectionPath).document(childName)
                .set(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }

    /*
         add data to child of a table
         param1: data to be store in table(Map<String, Object>).
         param2: collectionPath the collection name whome we want to add data(String)
         */
    public void addDataToFirestore(Map<String, Object> data, String collectionPath) {
        // Add a new document with a generated ID

        db.collection(collectionPath)
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }
    /*
      add data to child of a table
      @return : data to be store in table(Map<String, Object>).
      @param2: collectionPath the collection name whome we want to add data(String)
         */
    public List<Map<String, Object>> getDataFromFirestore(String collectionPath) {

        downloadDataFirestore(collectionPath);
        return listOfData;
    }
    /*
      add data to child of a table
      @return : data to be store in table(Map<String, Object>).
      @param2: collectionPath the collection name whome we want to add data(String)
      @param3: Collection's childName the child name whome we want to add data (String)
     */
    private void downloadChildDataFirestore(String collectionPath, String childName) {
        DocumentReference docRef=db.collection(collectionPath).document(childName);
        docRef.collection(collectionPath)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                listOfData.add(document.getData());//adding data to list
                                Log.d(TAG, document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
    }
    /*
      add data to child of a table
      @return : data to be store in table(Map<String, Object>).
      @param2: collectionPath the collection name whome we want to add data(String)
     */
    private void downloadDataFirestore(String collectionPath) {
        db.collection(collectionPath)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                listOfData.add(document.getData());//adding data to list
                                Log.d(TAG, document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.i(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
    }
}
