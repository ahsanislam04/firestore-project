package com.example.ahsanislam.androidfirestore;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String TAG ="aho" ;
    Button btn_Send;
    Button btn_retrieve;
    TextView tv_retrieved;
    EditText editText;
    FireStoreManager fireStoreManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_Send = (Button) findViewById(R.id.btn_send);
        btn_retrieve = (Button) findViewById(R.id.btn_retrieve);
        editText=(EditText)findViewById(R.id.editText);
        tv_retrieved=(TextView)findViewById(R.id.textView);


        fireStoreManager=new FireStoreManager();

    }

    public void onClickSend(View v) {
        Map<String,Object> data=new HashMap<>();
        data.put("Name",editText.getText().toString());


        fireStoreManager.addDataToFirestore(data,"User");
//        FirebaseFirestore.getInstance().collection("User")
//                .add(data)
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Toast.makeText(getApplicationContext(),"DocumentSnapshot added with ID: " + documentReference.getId(),Toast.LENGTH_SHORT).show();
//                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Toast.makeText(getApplicationContext(),"Error adding document\n"+e.toString(),Toast.LENGTH_SHORT).show();
//                        Log.w(TAG, "Error adding document", e);
//                    }
//                });
    }

    public void onClickRetrieve(View v) {
        List<Map<String,Object> > listData;

        listData=fireStoreManager.getDataFromFirestore("User");
        Log.i(TAG, "onClickRetrieve: "+listData.toString());
        for(Map<String,Object> datum:listData)
        {
            tv_retrieved.setText(datum.get("Name").toString());
        }
    }
}
